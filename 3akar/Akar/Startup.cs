﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Akar.Startup))]
namespace Akar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
