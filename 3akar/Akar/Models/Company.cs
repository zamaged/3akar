//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Akar.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobiles { get; set; }
        public string Whatsapps { get; set; }
        public string LandLines { get; set; }
        public bool Active { get; set; }
        public string UserId { get; set; }
        public byte[] CompanyLogo { get; set; }
        public string CompanyEmail { get; set; }
        public string Address { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
