﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Akar.Models;
using Microsoft.AspNet.Identity;

namespace Akar.Controllers
{
    public class CompaniesController : Controller
    {
        private Akar20180617043702_dbEntities db = new Akar20180617043702_dbEntities();

        // GET: Companies
        public ActionResult Index()
        {
            var companies = db.Companies.Include(c => c.AspNetUser);
            return View(companies.ToList());
        }

        public ActionResult Show(int id)
        {
            var imageData = db.Brokers.Find(id).Picture;

            return File(imageData, "image/jpg");
        }
        public ActionResult Approve(int id)
        {
            var Broker = db.Brokers.Find(id);
            Broker.Approved = true;
            db.AspNetUsers.Find(Broker.UserId).EmailConfirmed = true;
            db.SaveChanges();
            return View("ManageWaitingList");
        }
        public ActionResult Deny(int id)
        {
            var Broker = db.Brokers.Find(id);
            Broker.Approved = false;
            db.AspNetUsers.Find(Broker.UserId).EmailConfirmed = false;
            db.SaveChanges();
            return View("ManageWaitingList");
        }

        public ActionResult ManageWaitingList()
        {

            List<Broker> Brokers = null;
            var currentUser = User.Identity.GetUserId();
            var currentCompany = (db.Companies.Where(c => c.UserId == currentUser).ToList().Count > 0) ? db.Companies.Where(c => c.UserId == currentUser).First() : null;
            if (currentCompany != null)
                Brokers = db.Brokers.Where(c => c.CompanyId == currentCompany.Id).ToList();

            ViewBag.Broker = Brokers;
            return View();
        }


        // GET: Companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Mobiles,Whatsapps,LandLines,Active,UserId,CompanyLogo,CompanyEmail,Address")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Companies.Add(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", company.UserId);
            return View(company);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", company.UserId);
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Mobiles,Whatsapps,LandLines,Active,UserId,CompanyLogo,CompanyEmail,Address")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", company.UserId);
            return View(company);
        }

        // GET: Companies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company = db.Companies.Find(id);
            db.Companies.Remove(company);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
