﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Akar.Models;
using Microsoft.AspNet.Identity;
namespace Akar.Controllers
{
    public class AdvertisementsController : Controller
    {
        private Akar20180617043702_dbEntities db = new Akar20180617043702_dbEntities();

        // GET: Advertisements
        public ActionResult Index()
        {
            return View(db.Advertisements.ToList());
        }
        public ActionResult upload(string id ,string AdCode)
        {
            string[] s = id.Split('_');
            ViewBag.ImageName = s[0];
            ViewBag.adCode = s[1];
            return View();
        }

        [HttpPost]
        public ActionResult upload(string RoomName, bool C3d, string AdCode, HttpPostedFileBase file)
        {
            Adimage Image = new Adimage();
            Image.RoomName = RoomName;
            Image.C3d = C3d;
            Image.AdCode = AdCode;
            try
            {
                var files = Request.Files[0];
                var hpf = files.InputStream;
                if (hpf.Length > 0)
                {
                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                    {
                        hpf.CopyTo(ms);
                        Image.Image = ms.GetBuffer();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (ModelState.IsValid)
            {
                db.Adimages.Add(Image);
                db.SaveChanges();
                return Json(new
                {
                    status = "success2"
                });
            }
            return Json(new
            {
                status = "failure",
                formErrors = ModelState.Select(kvp => new { key = kvp.Key, errors = kvp.Value.Errors.Select(e => e.ErrorMessage) })
            });
        }
        public ActionResult ShowImgbyID(string id)
        {
            var imageData = db.Adimages.Where(c => c.Id.ToString() == id && c.RoomName != "Main").FirstOrDefault();
            if (imageData != null)
            {
                if (Request.IsAjaxRequest())
                {
                    var filennn = DateTime.Now.ToString().Replace("/", "").Replace(":", "") + imageData.RoomName + ".jpeg";
                    var filename = Path.GetFileName(filennn);
                    var path = Path.Combine(Server.MapPath("~/Content/assets/img/Temp/"), filennn);
                    FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                    fs.Write(imageData.Image, 0, imageData.Image.Length);
                    return Json("http://"+ Request.Url.Authority + "/Content/assets/img/Temp/" + filennn, JsonRequestBehavior.AllowGet);
                }
                
                return File(imageData.Image, "image/jpg");
            }
            else
            {
                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\assets\\img", "No_Image_Available.png");
                FileStream fs = null;
                try
                {
                    fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    FileStreamResult result = new FileStreamResult(fs, "image/png");

                    fs = null;
                    result.FileDownloadName = "image.png";
                    return result;
                }
                finally
                {
                    if (fs != null)
                        fs.Dispose();
                }

            }
        }
        public ActionResult ShowMain(string id)
        {
            var imageData = db.Adimages.Where(c => c.AdCode == id.ToString() && c.RoomName == "Main").FirstOrDefault();
            if (imageData != null)
                return File(imageData.Image, "image/jpg");
            else
            {
                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\assets\\img", "No_Image_Available.png");
                FileStream fs = null;
                try
                {
                    fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    FileStreamResult result = new FileStreamResult(fs, "image/png");

                        fs = null;
                    result.FileDownloadName = "image.png";
                    return result;
                }
                finally
                {
                    if (fs != null)
                        fs.Dispose();
                }

            }
        }
        // GET: Advertisements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }
        public ActionResult AddAdvertViewModel2()
        {
            ViewBag.Locations = db.PropertyLocations.Select(c => new { Id = c.Id, Name = c.Governorate + " - " + c.District + (string.IsNullOrEmpty(c.Project) ? "" : " - " + c.Project) }).ToList();
            ViewBag.types = db.PropertyTypes.Select(c => new { Id = c.Id, EName = c.EnglishName, AName = c.ArabicName }).ToList();
            ViewBag.AdCode = (db.Advertisements.ToList()[(db.Advertisements.ToList().Count-1)].Id+1).ToString() + Guid.NewGuid().ToString().Split('-')[3];

            return PartialView();
        }
        public ActionResult AddAdvertViewModel()
        {
            ViewBag.Locations = db.PropertyLocations.Select(c => new { Id = c.Id, Name = c.Governorate + " - " + c.District + (string.IsNullOrEmpty(c.Project) ? "" : " - " + c.Project) }).ToList();
            ViewBag.types = db.PropertyTypes.Select(c => new { Id = c.Id, EName = c.EnglishName, AName = c.ArabicName }).ToList();
            ViewBag.AdCode = (db.Advertisements.ToList()[(db.Advertisements.ToList().Count-1)].Id+1).ToString() + Guid.NewGuid().ToString().Split('-')[3];

            return PartialView();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult AddAdvertViewModel2([Bind(Include = "Id,AdTitle,PropertyLocationId,PropertyTypeId,Sale_Rent,Area,BedRooms,BathRooms,LivingRooms,MasterRoom,Reciption,DressingRooms,Floor,Elevator,Price,AdDescription,Paymentfacilities,Furnish,PaymentDuration,DownPayment,Code")] Advertisement advertisement)
        {
            var CurrentUser = User.Identity.GetUserName();
            var currentuserid = db.AspNetUsers.Where(c => c.UserName == CurrentUser); 
            var images = db.Adimages.Where(c => c.AdCode == advertisement.Code);
            foreach(var image in images)
            {
                image.AdId = advertisement.Id;
            }
            advertisement.UserId = currentuserid.FirstOrDefault()?.Id;
            if (ModelState.IsValid)
            {
                db.Advertisements.Add(advertisement);
                db.SaveChanges();
                return Json(new
                {
                    status = "success"
                });
            }
            return Json(new
            {
                status = "failure",
                formErrors = ModelState.Select(kvp => new { key = kvp.Key, errors = kvp.Value.Errors.Select(e => e.ErrorMessage) })
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HandleError]
        public ActionResult AddAdvertViewModel([Bind(Include = "Id,AdTitle,PropertyLocationId,PropertyTypeId,Sale_Rent,Area,BedRooms,BathRooms,LivingRooms,MasterRoom,Reciption,DressingRooms,Floor,Elevator,Price,AdDescription,Paymentfacilities,Furnish,PaymentDuration,DownPayment,Code")] Advertisement advertisement)
        {
            var CurrentUser = User.Identity.GetUserId();
            var images = db.Adimages.Where(c => c.AdCode == advertisement.Code);
            foreach(var image in images)
            {
                image.AdId = advertisement.Id;
            }
            advertisement.UserId = CurrentUser;
            if (ModelState.IsValid)
            {
                db.Advertisements.Add(advertisement);
                db.SaveChanges();
                return Json(new
                {
                    status = "success"
                });
            }
            return Json(new
            {
                status = "failure",
                formErrors = ModelState.Select(kvp => new { key = kvp.Key, errors = kvp.Value.Errors.Select(e => e.ErrorMessage) })
            });
        }

        // GET: Advertisements/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Advertisements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,PropertyLocationId,PropertyTypeId,Sale_Rent,Area,BedRooms,BathRooms,LivingRooms,MasterRoom,Reciption,DressingRooms,Floor,Elevator,Price,Description,Paymentfacilities,Furnish,PaymentDuration,DownPayment")] Advertisement advertisement)
        {
            if (ModelState.IsValid)
            {
                db.Advertisements.Add(advertisement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(advertisement);
        }

        // GET: Advertisements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // POST: Advertisements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,PropertyLocationId,PropertyTypeId,Sale_Rent,Area,BedRooms,BathRooms,LivingRooms,MasterRoom,Reciption,DressingRooms,Floor,Elevator,Price,Description,Paymentfacilities,Furnish,PaymentDuration,DownPayment")] Advertisement advertisement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertisement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(advertisement);
        }

        // GET: Advertisements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // POST: Advertisements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertisement advertisement = db.Advertisements.Find(id);
            db.Advertisements.Remove(advertisement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
