﻿using Akar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Akar.Controllers
{
    public class HomeController : Controller
    {
        private Akar20180617043702_dbEntities db = new Akar20180617043702_dbEntities();
        public ActionResult Index()
        {
            ViewBag.Locations = db.PropertyLocations.Select(c => new { Id = c.Id, Name = c.Governorate + " - " + c.District + (string.IsNullOrEmpty(c.Project) ? "" : " - " + c.Project) }).ToList();
            ViewBag.types = db.PropertyTypes.Select(c => new { Id = c.Id, EName = c.EnglishName, AName = c.ArabicName }).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Index(int? PropertyLocationId, int? PropertyTypeId, int? Sale_Sale)
        {
            var advertisement = db.Advertisements.Where(c => c.PropertyLocationId == PropertyLocationId && c.PropertyTypeId == PropertyTypeId && c.Sale_Rent == (Sale_Sale==0?c.Sale_Rent: Sale_Sale)).ToList();
            return Json(new
            {
                
                data =advertisement.Select(kvp => new { Id = kvp.Id, title = kvp.AdTitle, price =kvp.Price, Area=kvp.Area,Desc =kvp.AdDescription,Code=kvp.Code,type=kvp.Sale_Rent==1?"للإيجار": kvp.Sale_Rent == 2?"للبيع":"مطلوب للشراء" }),
                status = "success"
            });
        }


            public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}